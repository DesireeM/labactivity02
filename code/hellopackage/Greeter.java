package hellopackage;
import java.util.Scanner;
import java.util.Random;
import java.util.*;
import secondpackage.Utilities;

public class Greeter 
{
    public static void main (String [] args)
    {
        Scanner scan = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Enter an interger value: ");
        int input = scan.nextInt();

        Utilities u = new Utilities();
        System.out.println(u.doubleMe(input));
    }
}
